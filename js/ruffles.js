$(document).on('show.bs.modal', function (event) {
    if (!event.relatedTarget) {
        $('.modal').not(event.target).modal('hide');
    };
    if ($(event.relatedTarget).parents('.modal').length > 0) {
        $(event.relatedTarget).parents('.modal').modal('hide');
    };
});

$(document).on('shown.bs.modal', function (event) {
    if ($('body').hasClass('modal-open') == false) {
        $('body').addClass('modal-open');
    };
});


 
// });
$(document).ready(function() {
	$('.ir-arriba').click(function(){ 
		$('html, body').animate({scrollTop:0}, 'slow'); 
		return false;
	});
});

  $(document).ready(function(){
      $('.ir-abajo').click(function(){
         $('body,html').animate({scrollTop: 615}, 'slow');
         return false;
      });
   
  });

  $(document).ready(function(){
    $('.descubre').click(function(){
       $('body,html').animate({scrollTop: 615}, 'slow');
       return false;
    });
 
});



$(document).on("click", 'a.come.mobile', function(e) {
    e.preventDefault();
    var i = $(this).find("i");
    var m = $(this).parents("footer").find(".menu-footer-mobile");
    if (m.length) {
        m.slideToggle("fast", function() {
            if (i.hasClass("fa-chevron-down"))
                i.removeClass("fa-chevron-down").addClass("fa-chevron-up");
            else
                i.removeClass("fa-chevron-up").addClass("fa-chevron-down");
            //verificamos si el footer este en un modal y calculamos en base a ello
            var modal_parent = $(i).parents('.modal'); 
            if(modal_parent.length == 0){
              $("html, body").animate({
                  scrollTop: $(document).height()
              });
            }else{
              $(modal_parent).animate({
                  scrollTop: $(modal_parent).height()
              });
            }
        });
    }
});

